package Movie;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalField;
import java.util.Date;
import java.util.Objects;
import java.util.Observable;
import java.util.UUID;


@Entity
public class Moviee extends Observable {
    @Id
    private long id;
    private String movieName;
    private MovieType movieType;
    private String releaseDate;
    private String directorName;
    private Integer rating;
    private boolean isWatched;

    public Moviee(String movieName, MovieType movieType, String releaseDate, String directorName, boolean isWatched, Integer rating) {
        this.movieName = movieName;
        this.movieType = movieType;
        this.releaseDate = releaseDate;
        this.directorName = directorName;
        this.rating = rating;
        this.isWatched = isWatched;
    }

    public Moviee() {
    }

    @Override
    public String toString() {
            return "MovieName: " + movieName + "\nMovieType: " + movieType + "\nYear: " + releaseDate + "\nDirectorName: " + directorName + "\nRating: " + rating
                    +"\nWatched: " + isWatched + "\nId: " + id;
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public MovieType getMovieType() {
        return movieType;
    }

    public void setMovieType(MovieType movieType) {
        this.movieType = movieType;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getDirectorName() {
        return directorName;
    }

    public void setDirectorName(String directorName) {
        this.directorName = directorName;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public boolean isWatched() {
        return isWatched;
    }

    public void setWatched(boolean watched) {
        isWatched = watched;
        setChanged();
        notifyObservers();
    }
}
