package Interface;

import Movie.Moviee;
import lombok.Getter;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.util.*;


@Getter
public class MovieTableModel extends AbstractTableModel {

    private final String[] columnNames = new String[]{"Tytuł", "Gatunek", "Premiera", "Reżyser", "Ocena", "Obejrzany", "ID"};
    List<Moviee> movieSet;
    Map<Long, Moviee> movieMap;

    public static final MovieTableModel INSTANCE = new MovieTableModel();

    public MovieTableModel() {
        this.movieSet = new ArrayList<>();
        this.movieMap = new HashMap<>();
    }

    public void addMovie(Moviee moviee) {
        if (!movieMap.containsKey(moviee.getId())) {
            movieMap.put(moviee.getId(), moviee);
            movieSet.add(moviee);
            fireTableDataChanged();
        }
    }

    public void removeMovies(Moviee moviee) {
        if (movieMap.containsKey(moviee.getId())) {
            movieSet.remove(moviee);
            movieMap.remove(moviee.getId());
            fireTableDataChanged();
        }
    }

    public void clear() {
        movieSet.clear();
        movieMap.clear();
        fireTableDataChanged();
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == 5) return Boolean.class;
        else if (columnIndex == 4) return Integer.class;
        else if (columnIndex == 6) return Integer.class;
        else return super.getColumnClass(columnIndex);
    }

    @Override
    public String getColumnName(int column) {
        return this.columnNames[column];
    }

    @Override
    public int getRowCount() {
        return movieSet.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Moviee tmp = movieSet.get(rowIndex);
        switch (columnIndex) {
            case 0: // co ma wyświetlić kolumna 0
                return tmp.getMovieName();
            case 1: // co ma wyświetlić kolumna 1
                return tmp.getMovieType();
            case 2: // co ma wyświetlić kolumna 2
                return tmp.getReleaseDate();
            case 3: // co ma wyświetlić kolumna 3
                return tmp.getDirectorName();
            case 4: // co ma wyświetlić kolumna 3
                return tmp.getRating();
            case 5: {
                return tmp.isWatched();
            }
            case 6:
                return tmp.getId();
            default: // co ma wyświetlić każda inna kolumna
                return "unknown";
        }
    }

    @Override
    public void setValueAt(Object aValue, int row, int col) {
        if (col == 5) {
            Moviee tmp = movieMap.get(getValueAt(row, 6));
            tmp.setWatched(!tmp.isWatched());
        }
        fireTableCellUpdated(row, col);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex == 5) {
            return true;
        }
        return false;

    }


}
