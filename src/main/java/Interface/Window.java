package Interface;

import Movie.MovieType;
import Movie.Moviee;
import com.mysql.jdbc.CommunicationsException;
import com.toedter.calendar.JSpinnerDateEditor;
import lombok.Getter;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.JDBCConnectionException;
import org.hibernate.query.Query;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

@Getter
public class Window implements Observer {


    private JPanel panel1;
    private JTextField tytulField;
    private JRadioButton takRadioButton;
    private JTextField rezysertextField;
    private JComboBox gatunekcomboBox;
    private JSlider ocenaSlider1;
    private JButton dodajFilmButton;
    private JTable table1;
    private JSpinnerDateEditor daterSpinner;
    private JLabel NowyFilm;
    private JButton zapiszPlikButton;
    private JProgressBar zapiszprogressBar2;
    private JButton usuńFilmButton;
    private JRadioButton nieRadioButton;
    private JButton zaladujButton;
    private JProgressBar wczytajprogressBar1;
    public JComboBox comboBox1;
    public JButton uaktualnijFilmButton;
    public JButton wyczyśćPolaButton;
    public JCheckBox bazaDanychOnlineCheckBox;
    public JCheckBox plikLokalnyCheckBox;
    public JTextField wpiszSzukanyTytułTextField;
    public JLabel changes;
    public JButton filtrujButton;
    private MovieTableModel model;
    private int selectedIndexes;
    private int unsavedChanges = 0;

    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();


    private File file = new File("src/main/java/MovieData.txt");

    public Window() {
        this.model = MovieTableModel.INSTANCE;
        table1.setModel(model);
        table1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table1.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);
        TableRowSorter<MovieTableModel> tableRowSorter = new TableRowSorter<>(model);
        table1.setRowSorter(tableRowSorter);
//        table1.setCellSelectionEnabled(false);
        usuńFilmButton.setEnabled(false);
        uaktualnijFilmButton.setEnabled(false);
        zapiszprogressBar2.setVisible(false);
        dodajFilmButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Moviee m = new Moviee(tytulField.getText(), Movie.MovieType.valueOf(gatunekcomboBox.getSelectedItem().toString()), comboBox1.getSelectedItem().toString(), rezysertextField.getText(), takRadioButton.isSelected(), ocenaSlider1.getValue());
                m.setId(System.currentTimeMillis());
                if (m.getMovieName().equals("")) {
                    JOptionPane.showMessageDialog(panel1, "Wpisz tytuł, aby dodać ten film.");
                    return;
                } else if (m.getDirectorName().equals("")) {
                    JOptionPane.showMessageDialog(panel1, "Wpisz reżysera, aby dodać ten film.");
                    return;
                }
                for (Moviee mo : model.movieSet) {
                    if (m.getMovieName().equals(mo.getMovieName())) {
                        JOptionPane.showMessageDialog(panel1, "Ten film został już dodany.");
                        return;
                    }
                }
                model.addMovie(m);
                listen();
                model.fireTableDataChanged();
                unsavedChanges++;
                setChanges();
                resizeColumnsToFitContent();
            }
        });

        ListSelectionModel selectionModel = table1.getSelectionModel();
        selectionModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (table1.getSelectedRow() == -1) {
                    usuńFilmButton.setEnabled(false);
                    uaktualnijFilmButton.setEnabled(false);
                } else {
                    getNowyFilm().setText("Edytowany Film");
                    Long id = (Long) table1.getValueAt(table1.getSelectedRow(), 6);
                    Moviee tmp = model.movieMap.get(id);
                    updateEditingFields(tmp);

                }
            }
        });

        usuńFilmButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.removeMovies(getSelectedMovie());
                unsavedChanges++;
                setChanges();
                resizeColumnsToFitContent();
            }
        });


        zapiszPlikButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                zapiszPlikButton.setEnabled(false);
                zapiszPlikButton.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                WriteWithBar task = new WriteWithBar();
                task.done = false;
                task.addPropertyChangeListener(new PropertyChangeListener() {
                    @Override
                    public void propertyChange(PropertyChangeEvent evt) {
                        if ("progress" == evt.getPropertyName()) {
                            int progress = (Integer) evt.getNewValue();
                            zapiszprogressBar2.setValue(progress);
                        }
                    }
                });
                task.execute();
            }
        });


        zaladujButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ReadWithBar task = new ReadWithBar();
                task.done = false;
                task.addPropertyChangeListener(new PropertyChangeListener() {
                    @Override
                    public void propertyChange(PropertyChangeEvent evt) {
                        if ("progress" == evt.getPropertyName()) {
                            int progress = (Integer) evt.getNewValue();
                            zapiszprogressBar2.setValue(progress);
                        }
                    }
                });
                task.execute();
            }
        });
        wyczyśćPolaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                uaktualnijFilmButton.setEnabled(false);
                usuńFilmButton.setEnabled(false);
                clearFields();
                getNowyFilm().setText("Nowy Film");
            }
        });
        uaktualnijFilmButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (tytulField.getText().trim().equals("")) {
                    JOptionPane.showMessageDialog(panel1, "Uzupełnij tytuł, aby edytować film.");
                    return;
                } else if (rezysertextField.getText().trim().equals("")) {
                    JOptionPane.showMessageDialog(panel1, "Wpisz dane reżysera, aby uaktualnić ten film.");
                    return;
                }
                Moviee movieToUpdate = getSelectedMovie();
                movieToUpdate.setMovieName(tytulField.getText());
                movieToUpdate.setMovieType(Movie.MovieType.valueOf(gatunekcomboBox.getSelectedItem().toString()));
                movieToUpdate.setReleaseDate(comboBox1.getSelectedItem().toString());
                movieToUpdate.setDirectorName(rezysertextField.getText());
                movieToUpdate.setRating(ocenaSlider1.getValue());
                movieToUpdate.setWatched(takRadioButton.isSelected());

                model.fireTableDataChanged();
                unsavedChanges++;
                setChanges();
                resizeColumnsToFitContent();
            }
        });
        wpiszSzukanyTytułTextField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                wpiszSzukanyTytułTextField.setText("");
            }
        });


        wpiszSzukanyTytułTextField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                String title = wpiszSzukanyTytułTextField.getText();
                if (title.trim().length() == 0) {
                    tableRowSorter.setRowFilter(null);
                } else {
                    tableRowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + title));
                }
            }
        });

        //uncomment what's below to edit table change behavior

//        table1.getModel().addTableModelListener(new TableModelListener() {
//            public void tableChanged(TableModelEvent e) {
//
//            }
//        });
    }

    private void resizeColumnsToFitContent() {
        FontMetrics fontMetrics = table1.getFontMetrics(Font.decode(".SF NS Text"));
        int maxLength = 0;

        int[] max = {0, 1, 3};
        int[] min = {2, 4, 5, 6};

        for (int j : max) {
            maxLength = 0;
            for (int i = 0; i < table1.getRowCount(); i++) {
                int length = fontMetrics.stringWidth(table1.getValueAt(i, j).toString());
                if (length > maxLength) maxLength = length;
                table1.getColumnModel().getColumn(j).setMaxWidth(maxLength + 15);
            }
        }
        for (int j : min) {
            maxLength = fontMetrics.stringWidth(table1.getColumnName(j));
            table1.getColumnModel().getColumn(j).setMaxWidth(maxLength + 10);
        }
        table1.repaint();
    }

    private Moviee getSelectedMovie() {
        long id = Long.parseLong(table1.getValueAt(table1.getSelectedRow(), 6).toString());
        return model.movieMap.get(id);
    }

    private void updateEditingFields(Moviee tmp) {
        long id = tmp.getId();
        usuńFilmButton.setEnabled(true);
        uaktualnijFilmButton.setEnabled(true);
        tytulField.setText(model.getMovieMap().get(id).getMovieName());
        for (int i = 0; i < gatunekcomboBox.getItemCount(); i++) {
            if (gatunekcomboBox.getItemAt(i).toString().equals(String.valueOf(tmp.getMovieType()))) {
                gatunekcomboBox.setSelectedIndex(i);
            }
        }

        for (int i = 0; i < comboBox1.getItemCount(); i++) {
            if (comboBox1.getItemAt(i).toString().equals(String.valueOf(tmp.getReleaseDate()))) {
                comboBox1.setSelectedIndex(i);
            }
        }
        rezysertextField.setText(String.valueOf(tmp.getDirectorName()));
        ocenaSlider1.setValue(tmp.getRating());
        takRadioButton.setSelected(tmp.isWatched());
        nieRadioButton.setSelected(!tmp.isWatched());
    }

    class WriteWithBar extends SwingWorker<Void, Void> {
        boolean done;

        @Override
        protected void done() {
            zapiszPlikButton.setEnabled(true);
            zapiszPlikButton.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            zapiszprogressBar2.setValue(zapiszprogressBar2.getMinimum());
        }

        @Override
        protected Void doInBackground() throws Exception {

            unsavedChanges = 0;
            setChanges();
            changes.setVisible(false);
            if (!plikLokalnyCheckBox.isSelected() && !bazaDanychOnlineCheckBox.isSelected()) {
                JOptionPane.showMessageDialog(panel1, "Wybierz miejsce docelowe zapisu.");
                return null;
            }

            zapiszprogressBar2.setVisible(true);
            setProgress(20);


            if (plikLokalnyCheckBox.isSelected() && bazaDanychOnlineCheckBox.isSelected()) {
                Session session = sessionFactory.openSession();
                try (PrintWriter pw = new PrintWriter(new FileWriter(file, false))) {
                    session.beginTransaction();

                    Query<Moviee> query = session.createQuery("FROM Moviee");
                    List<Moviee> toDelete = query.list();

                    toDelete.removeAll(model.movieSet);

                    for (Moviee m : model.movieSet) {
                        pw.println(m);
                        pw.println("-separator-");
                        session.saveOrUpdate(m);
                    }

                    for (Moviee m : toDelete) {
                        session.remove(m);
                    }

                    pw.flush();

                    session.getTransaction().commit();

                } catch (IOException ioe) {
                    ioe.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                    if (e.getClass().equals(CommunicationsException.class)) {
                        Thread.sleep(2000);
                        writeToDatabase();
                    }
                } finally {
                    session.close();
                    sessionFactory.close();
                }
            } else if (plikLokalnyCheckBox.isSelected()) {
                try (PrintWriter pw = new PrintWriter(new FileWriter(file, false))) {


                    for (Moviee m : model.movieSet) {
                        pw.println(m);
                        pw.println("-separator-");
                    }

                    pw.flush();

                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            } else if (bazaDanychOnlineCheckBox.isSelected()) {

                writeToDatabase();

            }
            for (int i = 20; i < 101; i++) {
                Thread.sleep(6);
                setProgress(i);
            }
            listen();
            zapiszprogressBar2.setVisible(false);
            return null;
        }

        private void writeToDatabase() {
            Session session = sessionFactory.openSession();
            try {
                session.beginTransaction();


                for (Moviee m : model.movieSet) {
                    try {
                        session.saveOrUpdate(m);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                Query<Moviee> query = session.createQuery("FROM Moviee");
                List<Moviee> toDelete = query.list();

                toDelete.removeAll(model.movieSet);

                for (Moviee m : toDelete) {
                    session.remove(m);
                }

                session.getTransaction().commit();
            } catch (Exception e) {
                e.printStackTrace();
                session.getTransaction().rollback();
            } finally {
                session.close();
            }
        }
    }

    class ReadWithBar extends SwingWorker<Void, Void> {
        boolean done;

        @Override
        protected void done() {
            zaladujButton.setEnabled(true);
            zaladujButton.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            zapiszprogressBar2.setValue(zapiszprogressBar2.getMinimum());
        }

        @Override
        protected Void doInBackground() throws Exception {

            if (!plikLokalnyCheckBox.isSelected() && !bazaDanychOnlineCheckBox.isSelected()) {
                JOptionPane.showMessageDialog(panel1, "Wybierz źródło odczytu.");
                return null;
            }

            changes.setVisible(false);
            unsavedChanges = 0;
            zapiszprogressBar2.setVisible(true);
            zaladujButton.setEnabled(false);
            zaladujButton.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            model.clear();
            model.fireTableDataChanged();
            setProgress(0);

            for (int i = 0; i <= 100; i++) {
                Thread.sleep(6);
                setProgress(i);
            }

            if (bazaDanychOnlineCheckBox.isSelected()) {
                try {
                    readFromDB();
                } catch (Exception e) {
                    e.printStackTrace();
                    Thread.sleep(3000);
                    readFromDB();
                }
            }

            if (plikLokalnyCheckBox.isSelected()) {
                try (Scanner scan = new Scanner(file)) {
                    Moviee m = new Moviee("", MovieType.HORROR, "2001", "", true, 1);
                    while (scan.hasNextLine()) {
                        String line = scan.nextLine();
                        String[] lineArguments = line.split(":", 2);
                        String keyword = lineArguments[0];

                        if (keyword.contains("MovieName")) {
                            m = new Moviee();
                            m.setMovieName(lineArguments[1].trim());
                        } else if (keyword.contains("MovieType")) {
                            m.setMovieType(MovieType.valueOf(lineArguments[1].trim()));
                        } else if (keyword.contains("Year")) {
                            m.setReleaseDate(lineArguments[1].trim());
                        } else if (keyword.contains("DirectorName")) {
                            m.setDirectorName(lineArguments[1].trim());
                        } else if (keyword.contains("Watched")) {
                            m.setWatched(Boolean.parseBoolean(lineArguments[1].trim()));
                        } else if (keyword.contains("Rating")) {
                            m.setRating(Integer.parseInt(lineArguments[1].trim()));
                        } else if (keyword.contains("Id")) {
                            m.setId(Long.parseLong(lineArguments[1].trim()));
                        } else if (keyword.contains("separator")) {
                            model.addMovie(m);
                        }
                    }
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
            listen();
            model.fireTableDataChanged();
            resizeColumnsToFitContent();
            zapiszprogressBar2.setVisible(false);
            return null;
        }

        private void readFromDB() {
            Session session = sessionFactory.openSession();
            Query<Moviee> query = session.createQuery("FROM Moviee");
            List<Moviee> moviees = query.list();

            for (Moviee m : moviees) {
                model.addMovie(m);
            }
            session.close();
        }
    }

    public void listen() {
        model.movieSet.forEach(moviee -> moviee.addObserver(this));
    }

    @Override
    public void update(Observable o, Object arg) {
        unsavedChanges++;
        setChanges();
        updateEditingFields((Moviee) o);
    }

    public void clearFields() {
        tytulField.setText("");
        gatunekcomboBox.setSelectedIndex(1);
        comboBox1.setSelectedIndex(108);
        rezysertextField.setText("");
        ocenaSlider1.setValue(1);
        takRadioButton.setSelected(false);
        nieRadioButton.setSelected(false);
    }

    public void setChanges() {
        changes.setText("Niezapisanych zmian: " + unsavedChanges);
        changes.setVisible(true);
    }

}

