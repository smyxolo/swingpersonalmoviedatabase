package Interface;

import javax.swing.*;
import java.awt.*;

public class WindowFrame extends JFrame {
    private Window INSTANCE;

    public WindowFrame() throws HeadlessException {
        this.INSTANCE = new Window();
        this.setTitle("Moviee Database by MDB v1.0");
        setContentPane(INSTANCE.getPanel1());
        pack();
        setMinimumSize(new Dimension(1024, 600));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
